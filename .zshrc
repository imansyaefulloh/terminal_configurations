# Path to your oh-my-zsh installation.
export ZSH=/Users/imansyaefulloh/.oh-my-zsh

# DEFAULT USER
DEFAULT_USERNAME=$USER

# CUSTOM POWERLEVEL9K THEMES
TERM=xterm-256color
POWERLEVEL9K_MODE='awesome-fontconfig'

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon context dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status battery  time)
POWERLEVEL9K_TIME_FOREGROUND='black'
POWERLEVEL9K_TIME_BACKGROUND='yellow'
POWERLEVEL9K_TIME_FORMAT="%D{%H:%M:%S \uf073  %Y-%m-%d}"
POWERLEVEL9K_PROMPT_ON_NEWLINE=true

POWERLEVEL9K_OS_ICON_BACKGROUND="46"
POWERLEVEL9K_OS_ICON_FOREGROUND="black"

POWERLEVEL9K_BATTERY_CHARGING='yellow'
POWERLEVEL9K_BATTERY_CHARGED='green'
POWERLEVEL9K_BATTERY_DISCONNECTED='$DEFAULT_COLOR'
POWERLEVEL9K_BATTERY_LOW_THRESHOLD='10'
POWERLEVEL9K_BATTERY_LOW_COLOR='red'

POWERLEVEL9K_PHP_VERSION_BACKGROUND='blue'

POWERLEVEL9K_ALWAYS_SHOW_CONTEXT=true
POWERLEVEL9K_CONTEXT_TEMPLATE="%n"
POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND='161'
POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND='black'

POWERLEVEL9K_DIR_HOME_BACKGROUND='45'
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND='45'
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND='45'
POWERLEVEL9K_DIR_HOME_FOREGROUND='236'
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND='236'
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND='236'

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"
#ZSH_THEME=agnoster
#ZSH_THEME=steef
#ZSH_THEME=pygmalion
ZSH_THEME="powerlevel9k/powerlevel9k"
#ZSH_THEME="bullet-train"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

# User configuration

# export PATH="/usr/bin:/bin:/usr/sbin:/sbin:$PATH"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# ALIAS FOR PROJECTS
alias hh_cms="cd /Users/imansyaefulloh/Sites/codeigniter/halohola_cms"

# ALIAS FOR VIM
alias vi="vim"
alias svi="sudo vim"

# PHP DEVELOPMENT
alias pv="php -v"
alias xv="nginx -v"

alias xstat="sudo service nginx status"
alias xstart="sudo service nginx start"
alias xstop="sudo service nginx stop"
alias xres="sudo service nginx restart"

alias phpstat="sudo service php5-fpm status || sudo service php7.0-fpm status"
alias phpstart="sudo service php5-fpm start || sudo service php7.0-fpm start"
alias phpstop="sudo service php5-fpm stop || sudo service php7.0-fpm stop"
alias phpres="sudo service php5-fpm restart || sudo service php7.0-fpm restart"

# OH-MY-ZSH
alias editZsh="vi ~/.zshrc"
alias ez="vi ~/.zshrc"

alias desk="cd ~/Desktop/"
alias down="cd ~/Download/"

alias .="cd .."
alias ..="cd ../.."
alias ...="cd ../../.."
alias ....="cd ../../../.."
alias .....="cd ../../../../.."

alias ls="ls -la"
alias md="mkdir"
alias subl="open -a Sublime\ Text"
alias prof="vim ~/.bash_profile"
alias ref="source ~/.zshrc"
alias c="clear"
alias to="touch"
alias sites="cd /Users/imansyaefulloh/Sites/"

# homestead
alias editHomestead="vi ~/.homestead/Homestead.yaml"
alias openHomestead="cd ~/Vagrant/homestead"
alias hed="subl ~/.homestead/Homestead.yaml"

# Homestead 2.0.* with PHP 5.6
alias hup="cd ~/Homestead && vagrant up && vagrant ssh"
alias hsu="cd ~/Homestead && vagrant suspend"
alias hha="cd ~/Homestead && vagrant halt"
alias hssh="cd ~/Homestead && vagrant ssh"
alias hstat="cd ~/Homestead && vagrant status"

# Homestead 3.0.* with PHP 7.1
alias hup7="cd ~/Homestead-7 && vagrant up && vagrant ssh"
alias hsu7="cd ~/Homestead-7 && vagrant suspend"
alias hha7="cd ~/Homestead-7 && vagrant halt"
alias hstat7="cd ~/Homestead-7 && vagrant status"

# vagrant basic with custom box with Ubuntu14 or Ubuntu16
# alias vup14="cd ~/Vagrant && vagrant up && vagrant ssh"
# alias vsu14="cd ~/Vagrant && vagrant suspend"
# alias vha14="cd ~/Vagrant && vagrant halt"

# vagrant basic with custom box with Ubuntu14
alias vup14="cd ~/Vagrant && vagrant up ubuntu14 && vagrant ssh ubuntu14"
alias vsu14="cd ~/Vagrant && vagrant suspend"
alias vha14="cd ~/Vagrant && vagrant halt"

# vagrant basic with custom box with Ubuntu16
alias vup16="cd ~/Vagrant && vagrant up ubuntu16 && vagrant ssh ubuntu16"
alias vsu16="cd ~/Vagrant && vagrant suspend"
alias vha16="cd ~/Vagrant && vagrant halt"

# vagrant
alias vagup="cd ~/Vagrant && vagrant up"

# MacVIM
alias vim="mvim -v"

# Homestead 3.0.*
# alias homestead='function __homestead() { (cd ~/Homestead && vagrant $*); unset -f __homestead; }; __homestead'

export PATH=~/.composer/vendor/bin:$PATH

# LARAVEL 5
alias art="php artisan"
alias migref="php artisan migrate:refresh"
alias m:a="art make:auth"
alias m:t="art make:test"
alias m:tr="art make:transformer"
alias m:c="php artisan make:controller"
alias m:m="php artisan make:model"
alias m:mig="php artisan make:migration"
alias m:s="php artisan make:seeder"
alias m:r="php artisan make:request"
alias m:p="php artisan make:policy"
alias r:l="php artisan route:list"
alias r:c="php artisan route:cache"
alias r:cl="php artisan route:clear"
alias r:a="php artisan api:routes"

# Git
alias ga="git add"
alias gaa="git add ."
alias gc="git commit -m"
alias gp="git push"
alias gs="git status"
alias gl="git log"
alias glp="git log --pretty=oneline"

# Composer
alias comIn="composer install"
alias comUp="composer update"
alias comDump="composer dumpautoload"

# Ionic
alias ioninfo="ionic info"
alias ionserve="ionic serve"



# Testing 
# PHPUNIT
#alias phpunit="phpunit --colors"
alias pun="phpunit"


# PHP MySQL
alias startApache="sudo apachectl start"
alias stopApache="sudo apachectl stop"
alias restartApache="sudo apachectl restart"
alias editApache="sudo vi /etc/apache2/httpd.conf"
alias editVhosts='sudo vi /etc/apache2/extra/httpd-vhosts.conf'
alias editHosts="sudo vi /etc/hosts"
alias startMysql="sudo /usr/local/mysql/support-files/mysql.server start"
alias stopMysql="sudo /usr/local/mysql/support-files/mysql.server stop"
alias editVim='vi ~/.vimrc'

# Set default editor
export EDITOR=/usr/bin/mvim

#   extract:  Extract most know archives with one command
#   ---------------------------------------------------------
    extract () {
        if [ -f $1 ] ; then
          case $1 in
            *.tar.bz2)   tar xjf $1     ;;
            *.tar.gz)    tar xzf $1     ;;
            *.bz2)       bunzip2 $1     ;;
            *.rar)       unrar e $1     ;;
            *.gz)        gunzip $1      ;;
            *.tar)       tar xf $1      ;;
            *.tbz2)      tar xjf $1     ;;
            *.tgz)       tar xzf $1     ;;
            *.zip)       unzip $1       ;;
            *.Z)         uncompress $1  ;;
            *.7z)        7z x $1        ;;
            *)     echo "'$1' cannot be extracted via extract()" ;;
             esac
         else
             echo "'$1' is not a valid file"
         fi
    }

#   ---------------------------
#   4.  SEARCHING
#   ---------------------------

alias qfind="find . -name "                 # qfind:    Quickly search for file
ff () { /usr/bin/find . -name "$@" ; }      # ff:       Find file under the current directory
ffs () { /usr/bin/find . -name "$@"'*' ; }  # ffs:      Find file whose name starts with a given string
ffe () { /usr/bin/find . -name '*'"$@" ; }  # ffe:      Find file whose name ends with a given string

#   memHogsTop, memHogsPs:  Find memory hogs
#   -----------------------------------------------------
    alias memHogsTop='top -l 1 -o rsize | head -20'
    alias memHogsPs='ps wwaxm -o pid,stat,vsize,rss,time,command | head -10'

#   cpuHogs:  Find CPU hogs
#   -----------------------------------------------------
    alias cpu_hogs='ps wwaxr -o pid,stat,%cpu,time,command | head -10'

#   topForever:  Continual 'top' listing (every 10 seconds)
#   -----------------------------------------------------
    alias topForever='top -l 9999999 -s 10 -o cpu'

#   ttop:  Recommended 'top' invocation to minimize resources
#   ------------------------------------------------------------
#       Taken from this macosxhints article
#       http://www.macosxhints.com/article.php?story=20060816123853639
#   ------------------------------------------------------------
    alias ttop="top -R -F -s 10 -o rsize"

#   my_ps: List processes owned by my user:
#   ------------------------------------------------------------
    my_ps() { ps $@ -u $USER -o pid,%cpu,%mem,start,time,bsdtime,command ; }

#   ---------------------------
#   6.  NETWORKING
#   ---------------------------

alias myip='curl ip.appspot.com'                    # myip:         Public facing IP Address
alias netCons='lsof -i'                             # netCons:      Show all open TCP/IP sockets
alias flushDNS='dscacheutil -flushcache'            # flushDNS:     Flush out the DNS Cache
alias lsock='sudo /usr/sbin/lsof -i -P'             # lsock:        Display open sockets
alias lsockU='sudo /usr/sbin/lsof -nP | grep UDP'   # lsockU:       Display only open UDP sockets
alias lsockT='sudo /usr/sbin/lsof -nP | grep TCP'   # lsockT:       Display only open TCP sockets
alias ipInfo0='ipconfig getpacket en0'              # ipInfo0:      Get info on connections for en0
alias ipInfo1='ipconfig getpacket en1'              # ipInfo1:      Get info on connections for en1
alias openPorts='sudo lsof -i | grep LISTEN'        # openPorts:    All listening connections
alias showBlocked='sudo ipfw list'                  # showBlocked:  All ipfw rules inc/ blocked IPs

#   cleanupDS:  Recursively delete .DS_Store files
#   -------------------------------------------------------------------
    alias cleanupDS="find . -type f -name '*.DS_Store' -ls -delete"

#   finderShowHidden:   Show hidden files in Finder
#   finderHideHidden:   Hide hidden files in Finder
#   -------------------------------------------------------------------
    alias finderShowHidden='defaults write com.apple.finder ShowAllFiles TRUE'
    alias finderHideHidden='defaults write com.apple.finder ShowAllFiles FALSE'

    alias showHidden='defaults write com.apple.finder ShowAllFiles TRUE'
    alias hideHidden='defaults write com.apple.finder ShowAllFiles FALSE'

    alias showFile='defaults write com.apple.finder AppleShowAllFiles -boolean true ; killall Finder'
    alias hideFile='defaults write com.apple.finder AppleShowAllFiles -boolean false ; killall Finder'

# Setting PATH for Python 3.4
# The orginal version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.4/bin:${PATH}"
export PATH

PATH="/usr/local/bin:$PATH"

export PATH="~/.composer/vendor/bin:vendor/bin:$PATH"

export NVM_DIR="/Users/imansyaefulloh/.nvm"

export PATH="/usr/local/bin:$PATH"

# setup android_home
export ANDROID_HOME="/Users/imansyaefulloh/Library/Android/sdk"
export ANDROID_NDK="/Users/imansyaefulloh/ndk/android-ndk-r10e"
export PATH="$ANDROID_HOME/platform-tools:$PATH"
export PATH="$ANDROID_HOME/tools:$PATH"
export PATH="$ANDROID_NDK:$PATH"
export PATH="${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools"

export PATH="$HOME/.node/bin:$PATH"

export PATH=/Users/imansyaefulloh/.composer/vendor/bin:$PATH

export PATH="/usr/local/bin:$PATH"
export PATH="$HOME/.node/bin:$PATH"

export NVM_DIR=$(brew --prefix)/var/nvm
#source $(brew --prefix nvm)/nvm.sh

[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

export PATH="$(brew --prefix homebrew/php/php70)/bin:$PATH"

#export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

[[ -s "$HOME/.profile" ]] && source "$HOME/.profile"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

export EDITOR=/usr/local/Cellar/macvim/7.4/bin/mvim 

alias ctags="`brew --prefix`/bin/ctags"

export GOPATH=$HOME/Golang

export PATH="$HOME/.yarn/bin:$PATH"

DEFAULT_USER=$USER


export PATH="/usr/local/opt/node@6/bin:$PATH"
