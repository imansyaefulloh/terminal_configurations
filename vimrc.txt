set nocompatible              " be iMproved, required

so ~/.vim/plugins.vim

syntax enable

set backspace=indent,eol,start
set number
let mapleader = ','                 "Make coma as defautl leader"

"------------Visual----------------"
colorscheme molokai

set t_CO=256
set guifont=Fira\ Code:h17
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R

hi vertsplit guifg=bg guibg=bg

set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

"------------Split Management----------------"
set splitbelow
set splitright


nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-H> <C-W><C-H>
nmap <C-L> <C-W><C-L>

"------------Searching----------------"
"higlight searching"
set hlsearch
set incsearch

"------------Mappings---------------"
"Make it easy to edit vimrc file"
nmap <Leader>ev :tabedit ~/.vimrc<cr>

"Remove highlight"
nmap <Leader><space> :nohlsearch<cr>
"Display sidebar"
nmap <D-1> :NERDTreeToggle<cr>
nmap <D-r> :CtrlPBufTag<cr>
nmap <D-e> :CtrlPMRUFiles<cr>
nmap <D-p> :CtrlP<cr>

"------------Plugins (CtrlP)---------------"
let g:ctrlp_custom_ignore = 'node_modules\DS_Store\|git'
let g:ctrlp_show_hidden = 1

"Show hidden files"
let NERDTreeShowHidden=1

"GReplace"
set grepprg=ag
let g:grep_cmd_opts = '--line-numbers --noheading'

"Automatic Commands"

"automatic refresh vimrc file on save"
augroup autosourcing
    autocmd!
    autocmd BufWritePost .vimrc source %
augroup END
